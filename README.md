# Intuit Signup Page

From The Odin Project's [curriculum](http://www.theodinproject.com/courses/web-development-101/lessons/html-css)

# Easy setup with Docker

This project uses Webpack to build and serve the development files. All this is already configured for you in the docker image defined in `docker-compose.yml`.

Run `docker-compose up` to serve, and head over to your [browser](http://localhost:3000) to see the website.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details




